#!/bin/bash

echo -e "=== Wikid setup manager ===\n

	This script will install needed software if not isntalled already\n
		\n
		- creates python virtual environment with requirements\n
		- checks for Calibre ebook manager and eventually install it if not present\n
"

echo -n " Do you want to proceed [y/n]: "
read x
if [ "$x" == "y" ]; then
   echo ""
else
   exit;
fi


echo "Creating Python virtual environment .python"
python -m venv .python
source .python/bin/activate
pip install requests
pip install beautifulsoup4
pip install pycountry

has_calibe=$(dpkg-query -W -f='${Status}' calibre 2>/dev/null | grep -c "ok installed")

if [ "$has_calibre" == "0" ]; then
   echo -e "\n Calibre has to be installed";
   sudo apt-get install calibre
   exit;
else
   echo -e "\n Calibre is installed"
fi

echo -e "\n Creating launcher"
bash .app/create_launcher.sh
