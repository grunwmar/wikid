#!/bin/bash
upath=$(pwd)
echo " === Welcome to Wikid === "
echo -e -n " Project name: \e[38;5;118m"
read pname
echo -e '\e[m'

if [ -z "$pname" ]
then
      exit
else
      echo ""
fi

nano "./proj/$pname"

echo -n " Export list to PDF? [y/n]: "
read x

if [ -z $x ]
then
      x="n"
      echo ""
else
      echo ""
fi



if [ $x = "y" ] 
then
   echo " * * * * * * * exporting to PDF * * * * * * * "
   bash ./wikid --lt-pdf "$upath/proj/$pname"
   echo " "
   echo "  -= PDF =- "
   echo " "
else
  echo " * * * * * * * exporting to EPUB * * * * * * * "
  bash ./wikid -lt "$upath/proj/$pname"
  echo " "
  echo "  -= EPUB =- "
  echo " "
fi

lastdir=$(cat "$upath/.app/last_dir")

echo $upath

thunar "$lastdir"
echo -n "Empty list file? [y/n]: "
read x

if [ $x = "y" ]; then
   > "$upath/proj/$pname"
else
  echo ""
fi


