input_file=".app/tmp/$1_tmp.html"
conver_echo=$(ebook-convert "$input_file" "$2".epub --flow-size $3 $4 --disable-font-rescaling --level1-toc "//h:h2" --level2-toc "//h:h3" --epub-version $5 --output-profile "pocketbook_900" --language "$6" --title "$7" --authors "Wikipedia.org" )
paplay ".app/entry_done.bin" --volume=42768
rm "$input_file"
