import requests
from bs4 import BeautifulSoup
import base64
import sys
import re
import os
from urllib.parse import unquote
import datetime
import json
import hashlib
import csv
import check_duplicity
import pycountry as pc
LANGS = pc.languages 

def get_lang(code):
    result = None
    alp = len(code)
    for language in LANGS:
    
        if (alp == 2) and hasattr(language, 'alpha_2'):
            if language.alpha_2 == code:
                result = language
            
        if (alp == 3) and hasattr(language, 'alpha_3'):
            if language.alpha_3 == code:
                result = language
    
    return "-" if result is None else result.name


RTL_LANGS = ["he", "yi", "arb"]
TIME_STAMP = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
TIME_STAMP2 = datetime.datetime.now().strftime("%Y-%m-%d@%H-%M-%S")
TIME_STAMP_DAY = datetime.datetime.now().strftime("%Y-%m-%d")
TIME_STAMP_HOUR = datetime.datetime.now().strftime("%H-%M-%S")
CONFIG = None
ERR_FILE = "err_log.txt"

# log errors
def err_log(string, e=None):
    with open(ERR_FILE, "a+") as file:
        if e:
            file.write(string + "\n" + str(e) + "\n")
        else:
            file.write(string + "\n")

# log entries as TXT
def log_txt(lang, title, url):
    with open("./log/PLAIN_LOG.TXT", "a+") as log_file:
        log_file.write("[{}| {} ]= {}\n\n".format(lang, unquote(title, "utf-8"), unquote(url, "utf-8")))

def log_history(url):
    with open(".app/HISTORY", "a+") as log_file:
        log_file.write(url+"\n")

# log entries as csv
def log_csv(lang, title, url, export_format):
    loc_date = datetime.datetime.now().strftime(" %Y - %m - %d ")
    loc_time = datetime.datetime.now().strftime(" %H : %M : %S ")
    csv_filename="./log/TABLE_LOG.CSV"
    fieldnames = ['LANGUAGE', 'TITLE', 'LINK', 'EXPORTED_AS', 'DATE', 'TIME']

    if not os.path.isfile(csv_filename):
        with open(csv_filename, "w+", newline='\n') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

        
    with open(csv_filename, 'a+', newline='\n') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow(
                {'LANGUAGE': get_lang(lang.lower()).lower(),
                'TITLE': unquote(title, "utf-8"),
                'LINK': unquote(url, "utf-8"),
                'EXPORTED_AS': export_format.lower(),
                "DATE": loc_date,
                "TIME": loc_time,
                }
                )


# load config file
with open(".app/config.json", "r") as file:
    CONFIG = json.load(file)

# normalize yiddish orthography
def yiddish_filter(string):
    substitute_list = [
        ["יִ", "יִ"],
        ["וו", "װ"],
        ["וי", "ױ"],
        ["יי", "ײ"],
        ["ײַ", "ײַ"],
    ]
    for s in substitute_list:
        subst_regx = re.compile("{}".format(s[0]), 0)
        string = subst_regx.sub(s[1], string)
    return string


# create needed dirs to export files
def mk_dir(*dirs):
    path = ""
    for dir in dirs:
        path = os.path.join(path, dir)
        if not os.path.isdir(path):
            os.mkdir(path)
    return path


def read_file(filename):
    with open(filename, "r+") as file:
        return file.read()


# load images targeted in img src attribute and converts it into base64 data url
def load_img(isrc, count=""):
    try:
        response = requests.get(isrc)
        content_type = response.headers['content-type']
        enc = base64.standard_b64encode(response.content).decode("utf-8")
        return f"data:{content_type};base64,{enc}"
    except:
        return None


# download wiki article (by given title AND language code) from wiki api 
def get_wiki_article(lang, title):
    response = requests.get(f"https://{lang}.wikipedia.org/w/api.php?action=parse&prop=text&page={title}&format=json")
    json = response.json()
    title = json['parse']['title']
    text = json['parse']['text']['*']

    if lang.lower() in RTL_LANGS:
        dir = "rtl"
    else:
        dir = "ltr"

    page_style = read_file(".app/style-page.css")

    if CONFIG["pdf"]:
        direction_style = read_file(f".app/style-{dir}-pdf.css")
    else:
        direction_style = read_file(f".app/style-{dir}-epub.css")

    html = read_file(".app/template.html").format(
            title=title,
            text=text,
                style="\n" + page_style + "\n" + direction_style,
        )
    return html



def main(lang, title, images=False, epub=False, dir=None):
    title = unquote(title, "utf-8")
    n_title = title.replace('_', ' ')
    n_lang = lang.lower()

    TITLE = n_title + " (n_lang)"

    IMG_ATTR_TO_DELETE = ["decoding", "data-file-height", "data-file-width", "srcset", "style"]

    print(f'Downloading article \033[38;5;226m{n_title}\033[0m (\033[38;5;075m{n_lang}\033[0m)')

    soup = BeautifulSoup(get_wiki_article(lang, title), 'html.parser')

	# deletes span elements with class mw-editsection
    for item in soup.find_all("span", class_="mw-editsection"):
        item.extract()

	# removes style attribute from al div tags
    for item in soup.find_all("div"):
        if item.get('style'):
            del item['style']

	# deletes table elements with class sidebar
    for item in soup.find_all("table", class_="sidebar"):
        item.extract()
        print("\t\t\033[38;5;202mSidebar deleted\033[0m")

	# deletes table elements with class nomobile
    for item in soup.find_all("table", class_="nomobile"):
        item.extract()
        print("\t\t\033[38;5;202mNo mobile deleted\033[0m")

	# deletes table elements with class infobox
    if not CONFIG["info-box"]:
        for item in soup.find_all("table", class_="infobox"):
            item.extract()
            print("\t\t\033[38;5;202mInfobox deleted\033[0m")

	# deletes table elements with class metadata
    if not CONFIG["meta-data"]:
        for item in soup.find_all("table", class_="metadata"):
            item.extract()
            print("\t\t\033[38;5;202mMetadata deleted\033[0m")

    for item in soup.find_all("a"):
        item.unwrap()

	# delete all img tags 
    if not images:

        for item in soup.find_all("div", class_="thumb"):
            item.extract()

        for item in soup.find_all("img"):
            item.extract()

    else:
        image_count = 0
        failed_image_count = 0

        for item in soup.find_all("img"):

            url = item["src"]

            for a in IMG_ATTR_TO_DELETE:
                if item.get(a):
                    del item[a]

            if url[0:2] == "//":
                url = "https:" + url

            img_dataurl = load_img(url, count="{}".format(image_count))

            if img_dataurl:
                item["src"] = img_dataurl
                image_count += 1
            else:
                item.extract()
                failed_image_count += 1

        print(f"          Downloaded {image_count} images")
        print(f"          Failed {failed_image_count} images")

    if True:
        for item in soup.find_all("div", class_=["catlinks", "sisterproject", "toc"]):
            item.extract()

    file_data = yiddish_filter(str(soup))

	# creating unique string for running process to use it as part of filename in tmp folder
    hash_f = hashlib.sha256()
    hash_f.update("{}{}{}".format(title, file_data, TIME_STAMP).encode(encoding='UTF-8'))
    hashed = hash_f.hexdigest()

    process_name = title.replace("'", "") + "_["+lang.upper()+"]" + "_____" + hashed

	# creates temporary HTML to be used by Calibre converter
    with open(".app/tmp/{}_tmp.html".format(process_name), "w+") as file:
        file.write(file_data)



	# defininf converter attributes for converstion html > epub/pdf
    if epub:
        FOLDER = CONFIG["pdf-directory"] if CONFIG["pdf"] else CONFIG["epub-directory"]
        NAMED_FOLDER = os.path.basename(os.path.normpath(dir)[1:])
        NEW_FOLDER = mk_dir(os.path.join(FOLDER, NAMED_FOLDER))

        with open(".app/last_dir", "w+") as ldfile:
            ldfile.write(NEW_FOLDER)

        FILENAME_OUT = os.path.join(NEW_FOLDER, lang.upper() + "-" + title.replace("'", ""))
        print("          Running Calibre converter...")

        if CONFIG["embed-fonts"]:
            EMBED_FONTS = "--embed-all-fonts"
        else:
            EMBED_FONTS = ""

        FLOW_SIZE = CONFIG["flow-size"]

        if CONFIG["epub3"]:
            VERSION = "3"
        else:
            VERSION = "2"
        
        lang_name = get_lang(n_lang).lower()
        TITLE = f"{n_title}"

        if CONFIG["pdf"]:
           
            os.system(f"""bash .app/converter-pdf.sh '{process_name}' "{FILENAME_OUT}" {EMBED_FONTS} """)
            print(f'          Article saved as [\033[38;5;118m{FILENAME_OUT}\033[0m]')
        else:
         

            os.system(f"""bash .app/converter-epub.sh '{process_name}' "{FILENAME_OUT}" {FLOW_SIZE} {EMBED_FONTS} {VERSION} "{n_lang}" "{TITLE}" """)
            print(f'          Article saved as [\033[38;5;118m{FILENAME_OUT}\033[0m]')

    else:
        FOLDER = CONFIG["html-directory"]
        TS_FOLDER = mk_dir(FOLDER,  TIME_STAMP)
        FILENAME_OUT = os.path.join(TS_FOLDER, title + "_["+lang.upper()+"].html")
        with open(FILENAME_OUT, "w+") as file:
            file.write(str(soup))


# parses urls given in input list
def parse_list(text):
    re_url = re.compile("https\\:\\/\\/(\w+)\\.wikipedia\\.org\\/wiki\\/(.*)")
    url_list = text.split("\n")
    results = []
    for url in url_list:
        t = re_url.findall(url.strip())
        if len(t) > 0:
            results.append((t[0], url_list))
    return results


params = {
    "images": not CONFIG["no-images"],
    "epub": True,
}

if sys.argv[1] == "-i":
    print("\n  ***   ", end="")
    if len(sys.argv) == 4:
        main(sys.argv[2], sys.argv[3],
             images=params["images"], epub=params["epub"])

elif sys.argv[1] in ["-l", "-lt", "--lt-pdf"]:
    
    if sys.argv[1] == "--lt-pdf":
        CONFIG["pdf"] = True

	# reads given url list file and  runs main function with given parameters
    with open(sys.argv[2], "r") as file:
        tuples = parse_list(file.read())
        total = len(tuples)

        dir_name = os.path.splitext(sys.argv[2])[0]

        print("\n === Number of article to download is {} === ".format(total))

        for i, ((lang, title), url_list) in enumerate(tuples):
            try:
                print("\n \033[38;5;118m{:>3}\033[0m/\033[38;5;190m{:<3}\033[0m".format(i + 1, total), end="")

                if sys.argv[1] in ["-lt", "--lt-pdf"]:
                    main(lang, title, images=params["images"], epub=params["epub"], dir=dir_name)

                else:
                    main(lang, title, images=params["images"], epub=params["epub"], dir=".")

                print("        Done \033[38;5;190m{:>3}\033[0m %\n".format(round((i + 1) / total * 100)))
                


                actual_url = url_list[i]

                log_history(actual_url)
                log_txt(lang, title, actual_url)
                
                if CONFIG["pdf"]:
                    log_csv(lang, title, actual_url, export_format="PDF")
                else:
                    log_csv(lang, title, actual_url, export_format="EPUB")

            except Exception as e:
                print(f"Error occured in {lang}//{title}")
                print(e, "\n")
                err_log(f"{lang}//{title}", e=e)

print("History check...")
check_duplicity.check_duplicity(".app/HISTORY")
