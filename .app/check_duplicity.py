import sys, re, os


def check_duplicity(filename):
	URL_LIST = []
	CHECK_URL = re.compile("((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*")
	FILENAME = filename

	duplicities = 0
	count = 0

	with open(FILENAME, "r") as file_input:
		for url in file_input.readlines():
			if CHECK_URL.match(url.strip()) is not None:
				if url not in URL_LIST:
					URL_LIST.append(url)
				else:
					duplicities += 1
			count += 1

		with open(FILENAME, "w+") as file_output:
			for url in URL_LIST:
				file_output.write(url)

	print("Found {} duplicities in {} items.".format(duplicities, count))
